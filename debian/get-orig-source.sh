#!/bin/bash

PACKAGE_NAME=libgap-sage

set -e
set -u

usage() {
	echo "Usage: ${0##*/} --upstream-libgap-xversion <xver> --upstream-libgap-commit-epoch <commit epoch>"
	exit 1
	}

if [ "$#" != "4" ]; then
	usage
fi
if [  "$1" != "--upstream-libgap-xversion" -o "$3" != "--upstream-libgap-commit-epoch" ]; then
	usage
fi
UPSTREAM_XVERSION="$2"
UPSTREAM_COMMIT_EPOCH=$4

UPSTREAM_GAP_DOTVERSION=${UPSTREAM_XVERSION%%+*}
UPSTREAM_LGP_VERSION_NANO=${UPSTREAM_XVERSION##*+}

UPSTREAM_GAP_VERSION_MAJOR=${UPSTREAM_GAP_DOTVERSION%%.*}
UPSTREAM_GAP_VERSION_MINOR=${UPSTREAM_GAP_DOTVERSION#*.}
UPSTREAM_GAP_VERSION_MINOR=${UPSTREAM_GAP_VERSION_MINOR%%.*}
UPSTREAM_GAP_VERSION_MICRO=${UPSTREAM_GAP_DOTVERSION#*.*.}
UPSTREAM_GAP_VERSION_MICRO=${UPSTREAM_GAP_VERSION_MICRO%%.*}

UPSTREAM_COMMIT_DATE=${UPSTREAM_COMMIT_EPOCH%%g*}
UPSTREAM_COMMIT_ID=${UPSTREAM_COMMIT_EPOCH##*g}

UPSTREAM_VERSION="${UPSTREAM_GAP_VERSION_MAJOR}.${UPSTREAM_GAP_VERSION_MINOR}.${UPSTREAM_LGP_VERSION_NANO}"
UPSTREAM_GAP_RELEASE="${UPSTREAM_GAP_VERSION_MAJOR}r${UPSTREAM_GAP_VERSION_MINOR}"
UPSTREAM_GAP_RELEASE_PATCH=${UPSTREAM_GAP_VERSION_MICRO}
UPSTREAM_GAP_VERSION="${UPSTREAM_GAP_RELEASE}p${UPSTREAM_GAP_RELEASE_PATCH}"

##echo "UPSTREAM_XVERSION:     ${UPSTREAM_XVERSION} (libgap_version=${UPSTREAM_VERSION}; gap_version=${UPSTREAM_GAP_VERSION}; gap_dotversion=${UPSTREAM_GAP_DOTVERSION})"
##echo "UPSTREAM_COMMIT_EPOCH: ${UPSTREAM_COMMIT_EPOCH} (date=${UPSTREAM_COMMIT_DATE}; id=${UPSTREAM_COMMIT_ID})"
##exit 0

if [ ! -e debian/changelog ]; then
	echo 1>&2 "nonexitent debian/changelog file"
	exit 1
fi

UPSTREAM_REPO="https://bitbucket.org/vbraun/libgap"
UPSTREAM_GAPCORE_REPO="https://www.gap-system.org/pub/gap/gap${UPSTREAM_GAP_VERSION_MAJOR}core"

DEBIAN_SUFFIX="+${UPSTREAM_COMMIT_EPOCH}+dsx"

DEBIAN_UVERSION=${UPSTREAM_XVERSION}${DEBIAN_SUFFIX}
DEBIAN_ROOTFOLDERNAME="${PACKAGE_NAME}-${DEBIAN_UVERSION}.orig"
DEBIAN_TARBALLXZ="$(realpath ..)/${PACKAGE_NAME}_${DEBIAN_UVERSION}.orig.tar.xz"

GOS_TMPDIR=$(mktemp -d ./get-orig-source-XXXXXX)
GOS_TMPDIR=$(realpath $GOS_TMPDIR)
trap "/bin/rm -rf \"$GOS_TMPDIR\"" QUIT INT EXIT

GOS_WORKING_FOLDER=$(pwd)
cd ${GOS_TMPDIR}

message() {
	echo
	echo "-- -- $1"
	echo
	}


UPSTREAM_GITFOLDER=${GOS_TMPDIR}/GITFOLDER
UPSTREAM_GAPCOREFOLDER=${GOS_TMPDIR}/GAPCOREFOLDER
UPSTREAM_ROOTFOLDER=${GOS_TMPDIR}/unpacked
EXTRA_ROOTFOLDER=${GOS_TMPDIR}/pkgextra
GAPCORE_ROOTFOLDER=${GOS_TMPDIR}/gapcore

GAPCOREBALLFOLDERNAME="gap${UPSTREAM_GAP_RELEASE}"
GAPCOREZIPBALL="gap${UPSTREAM_GAP_VERSION}_nopackages.zip"

message "Cloning"

git clone https://bitbucket.org/vbraun/libgap.git ${UPSTREAM_GITFOLDER}


message "Fetching GAP-core zipball"

mkdir ${UPSTREAM_GAPCOREFOLDER}
cd ${UPSTREAM_GAPCOREFOLDER}
wget "${UPSTREAM_GAPCORE_REPO}/${GAPCOREZIPBALL}"


message "Extracting GAP-core relevant material"

mkdir ${GAPCORE_ROOTFOLDER}
cd ${GAPCORE_ROOTFOLDER}
unzip "${UPSTREAM_GAPCOREFOLDER}/${GAPCOREZIPBALL}" "${GAPCOREBALLFOLDERNAME}/src/*"
mv ${GAPCOREBALLFOLDERNAME} ${GAPCOREBALLFOLDERNAME}p${UPSTREAM_GAP_RELEASE_PATCH}
ln -sf ${GAPCOREBALLFOLDERNAME}p${UPSTREAM_GAP_RELEASE_PATCH} ${GAPCOREBALLFOLDERNAME}


message "Fetching extra material"

mkdir ${EXTRA_ROOTFOLDER}
cd ${EXTRA_ROOTFOLDER}
mkdir doc
cd doc
wget ${UPSTREAM_REPO}/downloads/libGAP.pdf


message "Packaging"

mkdir ${UPSTREAM_ROOTFOLDER}
cd ${UPSTREAM_GITFOLDER}
EFFECTIVE_UPSTREAM_COMMIT_UNIXEPOCH=$(git show -s --format=%ct ${UPSTREAM_COMMIT_ID})
EFFECTIVE_UPSTREAM_COMMIT_DATE=$(date -u +%Y%m%d -d @${EFFECTIVE_UPSTREAM_COMMIT_UNIXEPOCH})
if [ "${EFFECTIVE_UPSTREAM_COMMIT_DATE}" != "${UPSTREAM_COMMIT_DATE}" ]; then
	echo 1>&2 "inconsistent given commit date (expected ${UPSTREAM_COMMIT_DATE} but observed ${EFFECTIVE_UPSTREAM_COMMIT_DATE})"
	exit 64
fi
git archive --prefix="${DEBIAN_ROOTFOLDERNAME}/" --format=tar ${UPSTREAM_COMMIT_ID} | tar -C ${UPSTREAM_ROOTFOLDER} -xf -
git log --date iso ${UPSTREAM_COMMIT_ID} > ${EXTRA_ROOTFOLDER}/GitChangeLog

UPSTREAM_ROOTFOLDER=${UPSTREAM_ROOTFOLDER}/${DEBIAN_ROOTFOLDERNAME}
DEBIAN_ROOTFOLDER=${GOS_TMPDIR}/${DEBIAN_ROOTFOLDERNAME}

cd ${UPSTREAM_ROOTFOLDER}
set -f
## check
if [ ! -e patches/gap-${UPSTREAM_VERSION}.patch ]; then
	echo 1>&2 "nonexitent gap-${UPSTREAM_VERSION}.patch patch"
	exit 1
fi
## wipe out
rm --verbose --force -- .gitignore
rm --verbose --force -- .hgignore
rm --verbose --force -- m4/lt~obsolete.m4
rm --verbose --force -- m4/ltsugar.m4
rm --verbose --force -- m4/ltoptions.m4
rm --verbose --force -- m4/ltversion.m4
rm --verbose --force -- m4/libtool.m4
rm --verbose --force -- test-driver
rm --verbose --force -- depcomp
rm --verbose --force -- compile
rm --verbose --force -- install-sh
rm --verbose --force -- ltmain.sh
rm --verbose --force -- missing
rm --verbose --force -- INSTALL
rm --verbose --force -- config.sub
rm --verbose --force -- config.guess
##
find . -name 'Makefile.in' | xargs --no-run-if-empty rm --verbose --force
rm --verbose --force -- src/config.h.in
rm --verbose --force -- aclocal.m4
rm --verbose --force -- configure
## discard undesirable patches
find patches -type f -a -not -name gap-${UPSTREAM_VERSION}.patch | xargs --no-run-if-empty rm --verbose --force
## discard non-libGAP source files
for _g in $(find src/ -type f); do
	if $(grep --quiet --max-count=1 'LibGAP' $_g); then
	 :
	else
		case ${_g##*/} in
			gap_version.h|Makefile.am) ;;
			*) rm --verbose --force -- ${_g} ;;
		esac
	fi
done
#:
cd ${GOS_WORKING_FOLDER}
set +f
mv ${UPSTREAM_ROOTFOLDER} ${DEBIAN_ROOTFOLDER}

cp -prd ${GAPCORE_ROOTFOLDER} ${DEBIAN_ROOTFOLDER}
cp -prd ${EXTRA_ROOTFOLDER} ${DEBIAN_ROOTFOLDER}

GOS_TARBALL="${GOS_TMPDIR}/repacked.tar"
GOS_TARBALLXZ="${GOS_TARBALL}.xz"
tar -C ${GOS_TMPDIR} --owner=root --group=root --mode=a+rX --sort=name --create --file "${GOS_TARBALL}" ${DEBIAN_ROOTFOLDERNAME}
xz -9e < "${GOS_TARBALL}" > "${GOS_TARBALLXZ}"
mv "${GOS_TARBALLXZ}" "${DEBIAN_TARBALLXZ}"


message "Testing ${DEBIAN_TARBALLXZ##*/}"

xz --verbose --test ${DEBIAN_TARBALLXZ}

message "Printing information about ${DEBIAN_TARBALLXZ##*/}"

xz --verbose --list ${DEBIAN_TARBALLXZ}


message "Quitting"

##
## eos
