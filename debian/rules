#!/usr/bin/make -f
include /usr/share/dpkg/pkg-info.mk

CGAP = /usr/bin/gap

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

DEB_PKG_VERSION := $(DEB_VERSION)
DEB_PKG_DOTVERSION :=   $(word 1,$(subst +, ,$(DEB_PKG_VERSION)))
DEB_PKG_VERSION_NANO := $(word 2,$(subst +, ,$(DEB_PKG_VERSION)))
UPS_PKG_GIT_EPOCH :=    $(word 3,$(subst +, ,$(DEB_PKG_VERSION)))

DEB_PKG_VERSION_MAJOR := $(word 1,$(subst ., ,$(DEB_PKG_DOTVERSION)))
DEB_PKG_VERSION_MINOR := $(word 2,$(subst ., ,$(DEB_PKG_DOTVERSION)))
DEB_PKG_VERSION_MICRO := $(word 3,$(subst ., ,$(DEB_PKG_DOTVERSION)))

UPS_PKG_VERSION :=         $(DEB_PKG_VERSION_MAJOR).$(DEB_PKG_VERSION_MINOR).$(DEB_PKG_VERSION_NANO)
DEB_PKG_GAP_DOTVERSION :=  $(DEB_PKG_VERSION_MAJOR).$(DEB_PKG_VERSION_MINOR).$(DEB_PKG_VERSION_MICRO)
DEB_PKG_GAP_VERSION :=     $(DEB_PKG_VERSION_MAJOR)r$(DEB_PKG_VERSION_MINOR)p$(DEB_PKG_VERSION_MICRO)
DEB_PKG_LIBGAP_XVERSION := $(DEB_PKG_GAP_DOTVERSION)+$(DEB_PKG_VERSION_NANO)

DEB_BUILD_GAP_BUILD_DATETIME := $(shell LC_ALL=C date -u -d @$(SOURCE_DATE_EPOCH) +"%F %T (%Z) (Debian $(DEB_PKG_VERSION))")


export DEB_BUILD_MULTIARCH ?= $(shell dpkg-architecture -qDEB_BUILD_MULTIARCH)
DEB_BUILD_GAP_INFO_ARCH ?= $(shell echo 'Print(GAPInfo.Architecture);' | $(CGAP) -q -A -T)

export ACLOCAL_PATH=/usr/share/gnulib/m4

default:
	@uscan --no-conf --dehs --report || true

%:
	dh $@ --with autoreconf --builddirectory=_build

override_dh_auto_configure:
	# preamble: none
	# amble:
	dh_auto_configure -B _build -- --with-gap_system_arch=$(DEB_BUILD_GAP_INFO_ARCH)
	# postamble: based on the dist-hook target and the hacking section in README
	cp -t _build/src $(wildcard $(addprefix gapcore/gap$(DEB_PKG_GAP_VERSION)/src/,*.h *.c *.s))
	cp -t _build/src $(wildcard $(addprefix src/,*.h *.c))
	patch --directory _build/src --strip 1 < patches/gap-$(UPS_PKG_VERSION).patch
	scripts/libGAPify.py --modify _build/src
	sed -i -e 's/SyKernelVersion = "[0-9]*\.[0-9]*\.[0-9]*"/SyKernelVersion = "$(DEB_PKG_GAP_DOTVERSION)"/g' _build/src/system.c
	sed -i \
			-e 's/GAP_BUILD_VERSION  "libgap-[0-9]*\.[0-9]*\.[0-9]*"/GAP_BUILD_VERSION  "libgap-$(DEB_PKG_GAP_DOTVERSION)"/g' \
			-e 's/GAP_BUILD_DATETIME "\(.*\)"/GAP_BUILD_DATETIME "$(DEB_BUILD_GAP_BUILD_DATETIME)"/g' \
		_build/src/gap_version.h
	$(foreach _t, $(wildcard test/*.c) , sed -f debian/adhoc/scripts/libGAPify-test.sed $(_t) > _build/$(_t) ;)

override_dh_auto_configure-indep:
	@true

override_dh_auto_build-indep:
	@true

override_dh_auto_test-indep:
	@true

override_dh_installchangelogs:
	dh_installchangelogs --keep pkgextra/GitChangeLog

override_dh_compress-indep:
	dh_compress -X.pdf -Xexamples

get-orig-source:
	debian/get-orig-source.sh --upstream-libgap-xversion $(DEB_PKG_LIBGAP_XVERSION) --upstream-libgap-commit-epoch $(UPS_PKG_GIT_EPOCH)

get-info:
	@echo "DEB_PKG_VERSION:         $(DEB_PKG_VERSION)"
	@echo "UPS_PKG_VERSION:         $(UPS_PKG_VERSION)"
	@echo "UPS_PKG_GIT_EPOCH:       $(UPS_PKG_GIT_EPOCH)"
	@echo "DEB_PKG_GAP_DOTVERSION:  $(DEB_PKG_GAP_DOTVERSION)"
	@echo "DEB_PKG_GAP_VERSION:     $(DEB_PKG_GAP_VERSION)"
	@echo "DEB_PKG_LIBGAP_XVERSION: $(DEB_PKG_LIBGAP_XVERSION)"
